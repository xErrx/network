package lan.network.simulation.core;

public enum Type {

    INPUT, HIDDEN, OUTPUT;

}
