package lan.network.simulation.core;

import lan.network.simulation.entities.Actor;
import lan.network.simulation.entities.World;
import lan.network.simulation.framework.Framework;
import lan.network.simulation.framework.Settings;

public abstract class ActorNode extends AbstactNode {

    private Actor actor;

    public ActorNode(Type type, Activation activation) {
        super(type, activation);
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public Framework getFramework() {
        return getActor().getFramework();
    }
    
    public World getWorld(){
        return getFramework().getWorld();
    }

    public Settings getSettings(){
        return getFramework().getSettings();
    }
}
