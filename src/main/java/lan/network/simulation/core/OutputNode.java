package lan.network.simulation.core;

public abstract class OutputNode extends ActorNode {

    public OutputNode() {
        super(Type.OUTPUT, Activation.SIGMOID);
    }

    @Override
    public void addAxon(Connection axon) {
        // Do nothing
    }

}
