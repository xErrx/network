package lan.network.simulation.core;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstactNode implements Node {

    private final Type type;
    private final List<Connection> dendrites;
    private final List<Connection> axons;
    private final Activation activation;
    private boolean visited;
    private double memory;

    public AbstactNode(Type type, Activation activation) {
        this.type = type;
        this.dendrites = new ArrayList<>();
        this.axons = new ArrayList<>();
        this.activation = activation;
        this.visited = false;
        this.memory = 0D;
    }

    @Override
    public void addDendrite(Connection dendrite) {
        dendrites.add(dendrite);
    }

    @Override
    public void addAxon(Connection axon) {
        axons.add(axon);
    }

    @Override
    public double getvalue() {
        double value;
        if (visited) {
            value = memory;
        } else {
            visited = true;
            value = calculateValue();
            memory = value;
        }
        return value;
    }

    @Override
    public double calculateValue() {
        double sum = 0D;
        for (Connection dendrite : dendrites) {
            sum += dendrite.getInput().getvalue() * dendrite.getWeight();
        }
        return activation.calculate(sum);
    }

    @Override
    public void execute() {
        //Do nothing
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public int compareTo(Node o) {
        return Double.compare(this.getvalue(), o.getvalue());
    }

    @Override
    public boolean hasDendrites() {
        return !dendrites.isEmpty();
    }

    @Override
    public void clear() {
        visited = false;
    }

    @Override
    public int getNumberOfDendrites() {
        return dendrites.size();
    }

    @Override
    public int getNumberOfAxons() {
        return axons.size();
    }  
    
}
