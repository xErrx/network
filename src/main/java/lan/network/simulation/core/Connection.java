package lan.network.simulation.core;

public class Connection {

    private Node input;
    private Node output;
    private double weight;

    public Connection() {
    }

    public void setInput(Node input) {
        this.input = input;
        input.addAxon(this);
    }

    public void setOutput(Node output) {
        this.output = output;
        output.addDendrite(this);
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Node getInput() {
        return input;
    }

    public Node getOutput() {
        return output;
    }

    public double getWeight() {
        return weight;
    } 

}
