package lan.network.simulation.core;

public interface Activation {
    
    public static Activation LINEAR = (x) -> x;
    public static Activation SIGMOID = (x) -> 1D / (1D + Math.exp(-x));
    public static Activation TANH = (x) -> (Math.exp(x) - Math.exp(-x)) / (Math.exp(x) + Math.exp(-x));
    
    double calculate(double value);
    
}
