package lan.network.simulation.core;

public abstract class InputNode extends ActorNode {

    public InputNode() {
        super(Type.INPUT, Activation.LINEAR);
    }

    public abstract double calculateValue();

    @Override
    public void addDendrite(Connection dendrite) {
        // Do nothing
    }

}
