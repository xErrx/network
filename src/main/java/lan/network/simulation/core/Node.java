package lan.network.simulation.core;

public interface Node extends Comparable<Node>{
    
    public double getvalue();
    
    public double calculateValue();
    
    public void addDendrite(Connection dendrite);
    
    public void addAxon(Connection axon);
    
    public Type getType();
    
    public void execute();

    public boolean hasDendrites();
    
    public void clear();

    public int getNumberOfDendrites();

    public int getNumberOfAxons();

}
