package lan.network.simulation.core;

public class HiddenNode extends AbstactNode {

    public HiddenNode() {
        super(Type.HIDDEN, Activation.TANH);
    }
}
