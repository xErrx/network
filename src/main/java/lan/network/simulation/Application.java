package lan.network.simulation;

import lan.network.simulation.filters.FilterBuddy;
import lan.network.simulation.filters.FilterDistance;
import lan.network.simulation.filters.LayeredFilter;
import lan.network.simulation.filters.FilterLeft;
import lan.network.simulation.view.Presentation;
import lan.network.simulation.framework.Framework;
import lan.network.simulation.framework.Settings;
import lan.network.simulation.inputs.InputBias;
import lan.network.simulation.inputs.InputBuddy;
import lan.network.simulation.inputs.InputDirectionEast;
import lan.network.simulation.inputs.InputDirectionNorth;
import lan.network.simulation.inputs.InputDirectionSouth;
import lan.network.simulation.inputs.InputDirectionWest;
import lan.network.simulation.inputs.InputRadar;
import lan.network.simulation.inputs.InputRandom;
import lan.network.simulation.inputs.InputSonar;
import lan.network.simulation.output.OutputBackward;
import lan.network.simulation.output.OutputForward;
import lan.network.simulation.output.OutputRotateLeft;
import lan.network.simulation.output.OutputRotateRight;
import lan.network.simulation.output.OutputStrafeLeft;
import lan.network.simulation.output.OutputStrafeRight;
import lan.network.simulation.output.OutputWait;

public class Application {

    private static final long FRAME_RATE = 30;
    private static final long INTERVAL = 1000L / FRAME_RATE;

    private static final Settings BASIC = createSimpleSimulation();
    private static final Settings DISTANCE = createDistanceSimulation();
    private static final Settings BUDDY = createBuddySimulation();
    private static final Settings LAYERED = createLayeredSimulation();
    private static final Settings RADAR = createRadarSimulation();
    private static final Settings ADVANCED = createAdvancedSimulation();

    public static void main(String[] arguments) {

        Settings settings = BUDDY;
        LayeredFilter filter = new LayeredFilter();
        settings.setNeurons(-1);
        settings.setGenomes(-1);
        settings.setGenerations(500);
        settings.setXavier(true);
        
        
        filter.addFilter(new FilterBuddy());
        filter.addFilter(new FilterLeft(16));
        filter.setLevel(1);
        settings.setFilter(filter);
        
        settings.setModulus(settings.getGenerations() * 2);

        Framework framework = new Framework(settings);
        framework.prepare();

        Presentation presentation = new Presentation(framework);
        presentation.prepare();
        presentation.update();

        while (framework.hasGenerations()) {
            while (framework.hasRounds()) {
                long start = System.currentTimeMillis();
                //processInput();
                //update();
                framework.step();
                presentation.update();
                long end = System.currentTimeMillis();
                presentation.sleep(start + INTERVAL - end);
            }
            framework.next();
            presentation.update();
        }
        presentation.destroy();
        System.out.println(filter.getLevel());
    }

    private static Settings createSimpleSimulation() {
        Settings settings = new Settings();
        settings.setGenerations(10000);
        settings.setRounds(128);
        settings.setNeurons(-1);//
        settings.setGenomes(-1);//128
        settings.setModulus(100);
        settings.setMutations(5);
        settings.addNode(InputDirectionNorth.class);
        settings.addNode(InputRandom.class);
        settings.addNode(InputSonar.class);
        settings.addNode(OutputWait.class);
        settings.addNode(OutputForward.class);
        settings.addNode(OutputRotateLeft.class);
        settings.addNode(OutputRotateRight.class);
        LayeredFilter filter = new LayeredFilter();
        filter.addFilter(new FilterLeft(8));
        settings.setFilter(filter);
        return settings;
    }

    private static Settings createAdvancedSimulation() {
        Settings settings = new Settings();
        settings.setGenerations(10000);
        settings.setRounds(128);
        settings.setNeurons(-1);//
        settings.setGenomes(-1);//128
        settings.setModulus(100);
        settings.setMutations(5);
        settings.setRadar(5);
        settings.setMultithreaded(true);
        settings.setXavier(false);
        settings.addNode(InputBias.class);
        settings.addNode(InputRandom.class);
        settings.addNode(InputDirectionNorth.class);
        settings.addNode(InputDirectionEast.class);
        settings.addNode(InputDirectionSouth.class);
        settings.addNode(InputDirectionWest.class);
        settings.addNode(InputRadar.class);
        settings.addNode(InputBuddy.class);
        settings.addNode(OutputWait.class);
        settings.addNode(OutputForward.class);
        settings.addNode(OutputBackward.class);
        settings.addNode(OutputStrafeLeft.class);
        settings.addNode(OutputStrafeRight.class);
        settings.addNode(OutputRotateLeft.class);
        settings.addNode(OutputRotateRight.class);
        LayeredFilter filter = new LayeredFilter();
        filter.addFilter(new FilterLeft(8));
        settings.setFilter(filter);
        return settings;
    }

    private static Settings createDistanceSimulation() {
        Settings settings = new Settings();
        settings.setGenerations(10000);
        settings.setRounds(384);
        settings.setNeurons(-1);
        settings.setGenomes(-1);
        settings.setModulus(100);
        settings.setMutations(5);
        settings.addNode(InputDirectionNorth.class);
        settings.addNode(InputRandom.class);
        settings.addNode(InputSonar.class);
        settings.addNode(OutputWait.class);
        settings.addNode(OutputForward.class);
        settings.addNode(OutputBackward.class);
        settings.addNode(OutputRotateLeft.class);
        settings.addNode(OutputRotateRight.class);
        LayeredFilter filter = new LayeredFilter();
        filter.addFilter(new FilterLeft(16));
        filter.addFilter(new FilterDistance());
        filter.setLevel(2);
        settings.setFilter(filter);
        return settings;
    }

    private static Settings createBuddySimulation() {
        Settings settings = new Settings();
        settings.setGenerations(10000);
        settings.setRounds(384);
        settings.setNeurons(-1);
        settings.setGenomes(-1);
        settings.setModulus(100);
        settings.setMutations(5);
        settings.addNode(InputRandom.class);
        settings.addNode(InputDirectionNorth.class);
        settings.addNode(InputDirectionEast.class);
        settings.addNode(InputDirectionSouth.class);
        settings.addNode(InputDirectionWest.class);
        settings.addNode(InputSonar.class);
        settings.addNode(InputBuddy.class);
        settings.addNode(OutputWait.class);
        settings.addNode(OutputForward.class);
        settings.addNode(OutputBackward.class);
        settings.addNode(OutputStrafeLeft.class);
        settings.addNode(OutputStrafeRight.class);
        settings.addNode(OutputRotateLeft.class);
        settings.addNode(OutputRotateRight.class);
        LayeredFilter filter = new LayeredFilter();
        filter.addFilter(new FilterLeft(16));
        filter.addFilter(new FilterBuddy());
        filter.setLevel(2);
        settings.setFilter(filter);
        return settings;
    }

    private static Settings createRadarSimulation() {
        Settings settings = new Settings();
        settings.setGenerations(10000);
        settings.setRounds(384);
        settings.setNeurons(-1);
        settings.setGenomes(-1);
        settings.setModulus(100);
        settings.setMutations(5);
        settings.setRadar(5);
        settings.setMultithreaded(true);
        settings.setXavier(true);
        settings.addNode(InputBias.class);
        settings.addNode(InputRandom.class);
        settings.addNode(InputDirectionNorth.class);
        settings.addNode(InputDirectionEast.class);
        settings.addNode(InputDirectionSouth.class);
        settings.addNode(InputDirectionWest.class);
        settings.addNode(InputRadar.class);
        settings.addNode(InputBuddy.class);
        settings.addNode(OutputWait.class);
        settings.addNode(OutputForward.class);
        settings.addNode(OutputBackward.class);
        settings.addNode(OutputStrafeLeft.class);
        settings.addNode(OutputStrafeRight.class);
        settings.addNode(OutputRotateLeft.class);
        settings.addNode(OutputRotateRight.class);
        LayeredFilter filter = new LayeredFilter();
        filter.addFilter(new FilterLeft(16));
        filter.addFilter(new FilterBuddy());
        filter.setLevel(2);
        settings.setFilter(filter);
        return settings;
    }

    private static Settings createLayeredSimulation() {
        Settings settings = new Settings();
        settings.setGenerations(10000);
        settings.setRounds(384);
        settings.setNeurons(-1);
        settings.setGenomes(-1);
        settings.setModulus(100);
        settings.setMutations(5);
        settings.addNode(InputRandom.class);
        settings.addNode(InputDirectionNorth.class);
        settings.addNode(InputDirectionEast.class);
        settings.addNode(InputDirectionSouth.class);
        settings.addNode(InputDirectionWest.class);
        settings.addNode(InputSonar.class);
        settings.addNode(InputBuddy.class);
        settings.addNode(OutputWait.class);
        settings.addNode(OutputForward.class);
        settings.addNode(OutputBackward.class);
        settings.addNode(OutputStrafeLeft.class);
        settings.addNode(OutputStrafeRight.class);
        settings.addNode(OutputRotateLeft.class);
        settings.addNode(OutputRotateRight.class);
        LayeredFilter filter = new LayeredFilter();
        filter.addFilter(new FilterLeft(16));
        filter.addFilter(new FilterDistance());
        settings.setFilter(filter);
        return settings;
    }

}
