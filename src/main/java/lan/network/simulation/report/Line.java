package lan.network.simulation.report;

import java.util.List;
import lan.network.simulation.entities.Genome;

public class Line implements Comparable<Line> {

    private String code;
    private int amount;
    private List<Genome> genomes;

    public Line() {
        amount = 0;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public List<Genome> getGenomes() {
        return genomes;
    }

    public void setGenomes(List<Genome> genomes) {
        this.genomes = genomes;
    }

    @Override
    public int compareTo(Line other) {
        return -Integer.compare(this.amount, other.amount);
    }

    public void tick() {
        amount++;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        String line = System.lineSeparator();
        builder.append(line).append("----------------------------");
        builder.append(line).append("Amount: ").append(amount);
        builder.append(line).append("Lenght: ").append(genomes.size());
        double[] deviation = calcualteStandardDeviation();
        builder.append(line).append("Avg: ").append(deviation[0]);
        builder.append(line).append("Std: ").append(deviation[1]);
        for (Genome genome : genomes) {
            builder.append(line).append("\t").append(genome.getInput()).append("\t").append(genome.getWeight()).append("\t").append(genome.getOutput());
        }
        builder.append(line).append("----------------------------");
        return builder.toString();
    }

    private double[] calcualteStandardDeviation() {
        double sum = 0D;
        for (Genome genome : genomes) {
            sum += genome.getWeight();
        }
        double avg = sum / genomes.size();
        sum = 0D;
        for (Genome genome : genomes) {
            double value = Math.pow(genome.getWeight() - avg, 2);
            sum += value;
        }
        double variance = sum / genomes.size();
        double std = Math.sqrt(variance);
        double result[] = {avg, std};
        return result;
    }

}
