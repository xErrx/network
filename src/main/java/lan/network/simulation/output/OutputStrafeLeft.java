package lan.network.simulation.output;

import lan.network.simulation.core.OutputNode;

public class OutputStrafeLeft extends OutputNode {

    @Override
    public void execute() {
        getActor().strafeLeft();
    }

}
