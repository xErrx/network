package lan.network.simulation.output;

import lan.network.simulation.core.OutputNode;

public class OutputForward extends OutputNode {

    @Override
    public void execute() {
        getActor().moveForward();
    }

}
