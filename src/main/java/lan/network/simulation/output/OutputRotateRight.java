package lan.network.simulation.output;

import lan.network.simulation.core.OutputNode;

public class OutputRotateRight extends OutputNode {

    @Override
    public void execute() {
        getActor().rotateRight();
    }

}
