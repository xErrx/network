package lan.network.simulation.output;

import lan.network.simulation.core.OutputNode;

public class OutputStrafeRight extends OutputNode {

    @Override
    public void execute() {
        getActor().strafeRight();
    }
}
