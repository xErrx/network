package lan.network.simulation.output;

import lan.network.simulation.core.OutputNode;

public class OutputRotateLeft extends OutputNode {

    @Override
    public void execute() {
        getActor().rotateLeft();
    }

}
