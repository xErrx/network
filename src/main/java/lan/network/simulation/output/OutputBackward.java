package lan.network.simulation.output;

import lan.network.simulation.core.OutputNode;

public class OutputBackward extends OutputNode {

    @Override
    public void execute() {
        getActor().moveBackward();
    }

}
