package lan.network.simulation.framework;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import lan.network.simulation.core.ActorNode;
import lan.network.simulation.core.HiddenNode;
import lan.network.simulation.core.Node;
import lan.network.simulation.entities.Actor;
import lan.network.simulation.entities.Brain;
import lan.network.simulation.entities.Direction;
import lan.network.simulation.entities.Genome;
import lan.network.simulation.entities.Position;
import lan.network.simulation.entities.World;
import lan.network.simulation.filters.Filter;
import lan.network.simulation.report.Line;

public class Framework {

    private Settings settings;
    private Random random;
    private World world;
    private List<Actor> actors;
    private int generation;
    private int round;
    private int mutations;
    private ExecutorService executor;
    private boolean force;
    private boolean milestone;

    public Framework(Settings settings) {
        this.settings = settings;
        random = new Random();
        world = new World(settings.getWidth(), settings.getHeight());
        actors = new ArrayList<>();
        generation = 0;
        round = 0;
        mutations = 0;
        executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    public void prepare() {
        for (int i = 0; i < settings.getPopulation(); i++) {
            Actor actor = new Actor();
            actor.setFramework(this);
            actor.setIndex(i);
            actor.setSeed(random.nextLong());
            addPosition(actor);
            addDirection(actor);
            addBrain(actor);
            addGenomes(actor);
            actors.add(actor);
            //System.out.println(actor);
        }
    }

    public void next() {
        List<Line> report = map(actors);
        Line best = report.get(0);
        System.out.println(best);

        List<Actor> survivors = new ArrayList<>();
        Filter filter = settings.getFilter();
        for (Actor actor : actors) {
            if (filter.accept(actor)) {
                survivors.add(actor);
            }
        }
        System.out.println("gen: " + generation + " survivors: " + survivors.size() + " mutations: " + mutations + " strains: " + report.size());
        //Repopulate
        if (survivors.isEmpty()) {
            force = true;
            survivors = new ArrayList<>(actors);
        } else {
            force = false;
        }
        generation++;
        clear();

        int multipier = settings.getPopulation() / survivors.size();
        int amount = multipier * survivors.size();

        while (actors.size() < settings.getPopulation()) {
            Actor parent;
            if (actors.size() < amount) {
                parent = next(survivors);
            } else {
                parent = random(survivors);
            }
            Actor child = new Actor();
            child.setFramework(this);
            child.setIndex(parent.getIndex());
            child.setSeed(parent.getSeed());
            addPosition(child);
            addDirection(child);
            addBrain(child);
            List<Genome> genomes = parent.getGenomes();
            int marker = determinateMarker(genomes);
            for (int index = 0; index < genomes.size(); index++) {
                Genome genome = genomes.get(index);
                if (index == marker) {
                    Genome mutation = modify(genome, child);
                    child.add(mutation);
                    mutations++;
                } else {
                    child.add(genome);
                }
            }
            actors.add(child);
        }
    }

    private int determinateMarker(List<Genome> genomes) {
        int marker;
        if (force || mutation()) {
            marker = random.nextInt(genomes.size());
        } else {
            marker = -1;
        }
        return marker;
    }

    private Genome modify(Genome genome, Actor child) {
        int input = genome.getInput();
        double weight = genome.getWeight();
        int output = genome.getOutput();
        int value = random.nextInt(3);
        switch (value) {
            case 0:
                input = randomNode(child);
                break;
            case 1:
                weight = randomWeight();
                break;
            case 2:
                output = randomNode(child);
                break;
            default:
                throw new IllegalArgumentException("Invalid mutation");
        }
        Genome mutation = new Genome(input, weight, output);
        return mutation;
    }

    private Actor random(List<Actor> survivors) {
        int index = random.nextInt(survivors.size());
        Actor parent = survivors.get(index);
        return parent;
    }

    private int randomNode(Actor child) {
        List<Node> nodes = child.getBrain().getNodes();
        return random.nextInt(nodes.size());
    }

    private double randomWeight() {
        int negative = random.nextInt(2);
        double weight = random.nextDouble();
        return negative == 0 ? weight : -weight;
    }

    private void clear() {
        round = 0;
        actors.clear();
        world.clear();
        mutations = 0;
    }

    private void addGenomes(Actor actor) {
        List<Node> nodes = actor.getBrain().getNodes();
        int genomes = settings.getGenomes();
        if (genomes < 0) {
            double xavier;
            xavier = settings.isXavier() ? Math.sqrt(1D / actor.getBrain().getInputNodes().size()) : 1D;
            for (Node in : actor.getBrain().getInputNodes()) {
                for (Node out : actor.getBrain().getHiddenNodes()) {
                    int input = nodes.indexOf(in);
                    double weight = randomWeight() * xavier;
                    int output = nodes.indexOf(out);
                    Genome genome = new Genome(input, weight, output);
                    actor.add(genome);
                }
            }
            xavier = settings.isXavier() ? Math.sqrt(1D / actor.getBrain().getHiddenNodes().size()) : 1D;
            for (Node in : actor.getBrain().getHiddenNodes()) {
                for (Node out : actor.getBrain().getOutputNodes()) {
                    int input = nodes.indexOf(in);
                    double weight = randomWeight() * xavier;
                    int output = nodes.indexOf(out);
                    Genome genome = new Genome(input, weight, output);
                    actor.add(genome);
                }
            }
        } else {
            for (int k = 0; k < genomes; k++) {
                int input = random.nextInt(nodes.size());
                double weight = randomWeight();
                int output = random.nextInt(nodes.size());
                Genome genome = new Genome(input, weight, output);
                actor.add(genome);
            }
        }

    }

    private void addDirection(Actor actor) {
        Direction[] directions = Direction.values();
        int number = random.nextInt(directions.length);
        Direction direction = directions[number];
        actor.setDirection(direction);
    }

    private void addPosition(Actor actor) {
        Position position;
        do {
            int x = random.nextInt(world.getWidth());
            int y = random.nextInt(world.getHeight());
            position = new Position(x, y);
        } while (world.isOccupied(position));
        actor.setPosition(position);
        world.add(actor);
    }

    private void addBrain(Actor actor) {
        for (Class<? extends ActorNode> node : settings.getNodes()) {
            try {
                Constructor<? extends ActorNode> constructor = node.getConstructor();
                ActorNode instance = constructor.newInstance();
                instance.setActor(actor);
                actor.add(instance);
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(Framework.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        int neurons = settings.getNeurons();
        if (neurons < 0) {
            int input = actor.getBrain().getInputNodes().size();
            int output = actor.getBrain().getOutputNodes().size();
            neurons = 0;
            while (neurons * neurons < input * output) {
                neurons++;
            }
        }
        for (int j = 0; j < neurons; j++) {
            actor.add(new HiddenNode());
        }
    }

    public void step() {
        if (settings.isMultithreaded()) {
            List<Future<?>> tasks = new ArrayList<>();
            for (Actor actor : actors) {
                tasks.add(executor.submit(actor));
            }
            for (Future<?> task : tasks) {
                try {
                    task.get();
                } catch (InterruptedException | ExecutionException ex) {
                    Logger.getLogger(Framework.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            for (Actor actor : actors) {
                actor.execute();
                actor.clear();
            }
        } else {
            for (Actor actor : actors) {
                actor.action();
            }
        }
        round++;
    }

    public World getWorld() {
        return world;
    }

    public List<Actor> getActors() {
        return Collections.unmodifiableList(actors);
    }

    public int getGenomes() {
        return settings.getGenomes();
    }

    public boolean hasRounds() {
        return round < settings.getRounds();
    }

    public boolean hasGenerations() {
        return generation < settings.getGenerations();
    }

    public boolean isActive() {
        return generation % settings.getModulus() == 0;
    }

    public Settings getSettings() {
        return settings;
    }

    public int getGeneration() {
        return generation;
    }

    public Actor findBuddy(Actor actor) {
        Position position = actor.getPosition();
        Direction direction = actor.getDirection();
        Position movement = direction.getForward();
        Position destination = position.add(movement);
        return getWorld().findActor(destination);
    }

    private boolean mutation() {
        int number = random.nextInt(settings.getPopulation());
        return number < settings.getMutations();
    }

    private Actor next(List<Actor> survivors) {
        Actor survivor = survivors.remove(0);
        survivors.add(survivor);
        return survivor;
    }

    public void setMilestone(boolean milestone) {
        this.milestone = milestone;
    }

    public boolean isMilestone() {
        return milestone;
    }

    private List<Line> map(List<Actor> actors) {
        Map<String, Line> map = new HashMap<>();
        for (Actor actor : actors) {
            String code = actor.getCode();
            Line line = map.get(code);
            if (line == null) {
                line = new Line();
                line.setCode(code);
                line.setGenomes(actor.getGenomes());
                map.put(code, line);
            }
            line.tick();
        }
        List<Line> lines = new ArrayList(map.values());
        Collections.sort(lines);
        return lines;
    }

}
