package lan.network.simulation.framework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lan.network.simulation.core.ActorNode;
import lan.network.simulation.filters.Filter;
import lan.network.simulation.filters.FilterDirection;

public class Settings {

    private int width;
    private int height;
    private int population;
    private int rounds;
    private int generations;
    private int neurons;
    private int genomes;
    private int mutations;
    private int modulus;
    private int radar;
    private boolean multithreaded;
    private Filter filter;
    private List<Class<? extends ActorNode>> nodes;
    private boolean xavier;

    public Settings() {
        this(128, 128, 1000, 300, 10000, 1, 4, 10000, 1);
    }

    public Settings(int width, int height, int population, int rounds, int generations, int neurons, int genomes, int mutations, int modulus) {
        this.width = width;
        this.height = height;
        this.population = population;
        this.rounds = rounds;
        this.generations = generations;
        this.neurons = neurons;
        this.genomes = genomes;
        this.mutations = mutations;
        this.modulus = modulus;
        this.filter = new FilterDirection();
        this.nodes = new ArrayList<>();
        this.radar = 2;
        this.multithreaded = false;
        this.xavier = false;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    public int getGenerations() {
        return generations;
    }

    public void setGenerations(int generations) {
        this.generations = generations;
    }

    public int getNeurons() {
        return neurons;
    }

    public void setNeurons(int neurons) {
        this.neurons = neurons;
    }

    public int getGenomes() {
        return genomes;
    }

    public void setGenomes(int genomes) {
        this.genomes = genomes;
    }

    public int getMutations() {
        return mutations;
    }

    public void setMutations(int mutations) {
        this.mutations = mutations;
    }

    public int getModulus() {
        return modulus;
    }

    public void setModulus(int modulus) {
        this.modulus = modulus;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public void addNode(Class<? extends ActorNode> node) {
        this.nodes.add(node);
    }

    public List<Class<? extends ActorNode>> getNodes() {
        return Collections.unmodifiableList(nodes);
    }

    public int getRadar() {
        return radar;
    }

    public void setRadar(int radar) {
        this.radar = radar;
    }

    public boolean isMultithreaded() {
        return multithreaded;
    }

    public void setMultithreaded(boolean multithreaded) {
        this.multithreaded = multithreaded;
    }

    public boolean isXavier() {
        return xavier;
    }

    public void setXavier(boolean xavier) {
        this.xavier = xavier;
    }

    

}
