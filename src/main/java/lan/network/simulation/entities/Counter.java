package lan.network.simulation.entities;

public class Counter {

    private int number;

    public Counter() {
        number = -1;
    }

    public int getNumber() {
        return number;
    }

    public void increase() {
        number++;
    }

}
