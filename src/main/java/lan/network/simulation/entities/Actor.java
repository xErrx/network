package lan.network.simulation.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import lan.network.simulation.core.ActorNode;
import lan.network.simulation.core.Node;
import lan.network.simulation.framework.Framework;

public class Actor extends Observable implements Runnable{

    private Framework framework;
    private int index;
    private long seed;
    private Position position;
    private Direction direction;
    private Brain brain;
    private List<Genome> genomes;
    private String code;

    public Actor() {
        brain = new Brain();
        genomes = new ArrayList<>();
        code = null;
    }
    public Framework getFramework() {
        return framework;
    }
    public void setFramework(Framework framework) {
        this.framework = framework;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public void add(Genome genome) {
        genomes.add(genome);
        brain.wire(genome);
    }

    public List<Genome> getGenomes() {
        return Collections.unmodifiableList(genomes);
    }
    
    @Override
    public String toString() {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (Genome genome : genomes) {
            builder.add(genome.toString());
        }
        JsonObject object = Json.createObjectBuilder()
                .add("index", index)
                .add("seed", seed)
                .add("x", getPosition().getX())
                .add("y", getPosition().getY())
                .add("direction", String.valueOf(direction))
                .add("genomes", builder)
                .build();
        return object.toString();
    }

    
    public void calculate(){
        List<Node> nodes = brain.getActiveOutputNodes();
        //System.out.println(this);
        for (Node node : nodes) {
            node.getvalue();
            //System.out.println(node.getClass().getSimpleName() + ": " + node.getvalue());
        }

    }
    
    public void execute(){
        List<Node> nodes = brain.getActiveOutputNodes();
        if (nodes.isEmpty()) {
            //Do nothing
        } else {
            double max = -10D;
            Node best = null;
            for (Node node : nodes) {
                double value = node.getvalue();
                if(value > max){
                    best = node;
                    max = value;
                }
            }
            best.execute();
        }
    }
    
    public void action() {
        calculate();
        execute(); 
        clear();
    }

    public void moveForward() {
        position = position.add(direction.getForward());
        setChanged();
        notifyObservers();
    }

    public void moveBackward() {
        position = position.add(direction.getBackward());
        setChanged();
        notifyObservers();
    }

    public void strafeLeft() {
        position = position.add(direction.getBackward());
        setChanged();
        notifyObservers();
    }

    public void strafeRight() {
        position = position.add(direction.getRight());
        setChanged();
        notifyObservers();
    }

    public void rotateLeft() {
        direction = direction.rotateLeft();
    }

    public void rotateRight() {
        direction = direction.rotateRight();
    }

    public Brain getBrain() {
        return brain;
    }

    public void clear() {
        for (Node node : brain.getNodes()) {
            node.clear();
        }
    }

    @Override
    public void run() {
        calculate();
    }

    public String getCode() {
        if (code == null) {
            StringBuilder builder = new StringBuilder();
            for (Genome genome : genomes) {
                builder.append("=");
                builder.append(genome.toString());
            }
            builder.append("=");
            code = builder.toString();
        }
        return code;
    }

    public void add(Node node) {
        brain.add(node);
    }

    

}
