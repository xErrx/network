package lan.network.simulation.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lan.network.simulation.core.Connection;
import lan.network.simulation.core.Node;
import lan.network.simulation.core.Type;

public class Brain {

    private List<Node> nodes;
    private List<Node> active;

    public Brain() {
        nodes = new ArrayList<>();
        active = null;
    }

    public void add(Node node) {
        nodes.add(node);
    }

    public List<Node> getNodes() {
        return Collections.unmodifiableList(nodes);
    }

    public void wire(Genome genome) {
        Connection connection = new Connection();
        connection.setInput(nodes.get(genome.getInput()));
        connection.setWeight(genome.getWeight());
        connection.setOutput(nodes.get(genome.getOutput()));
    }

    public int[] getDistribution() {
        int[] distribution = new int[3];
        for (int i = 0; i < distribution.length; i++) {
            distribution[i] = 0;
        }
        for (Node node : nodes) {
            int index = node.getType().ordinal();
            distribution[index] += node.getNumberOfDendrites() + node.getNumberOfAxons();
        }
        return distribution;
    }

    public List<Node> getActiveOutputNodes() {
        if (active == null) {
            active = new ArrayList<>();
            for (Node node : nodes) {
                if (node.getType() == Type.OUTPUT && node.hasDendrites()) {
                    active.add(node);
                }
            }
        }
        return active;
    }

    public List<Node> findNodes(Type type) {
        List<Node> result = new ArrayList<>();
        for (Node node : nodes) {
            if (node.getType() == type) {
                result.add(node);
            }
        }
        return result;
    }

    public List<Node> getInputNodes() {
        return findNodes(Type.INPUT);
    }

    public List<Node> getHiddenNodes() {
        return findNodes(Type.HIDDEN);
    }

    public List<Node> getOutputNodes() {
        return findNodes(Type.OUTPUT);
    }

}
