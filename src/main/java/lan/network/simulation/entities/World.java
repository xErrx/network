package lan.network.simulation.entities;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

public class World implements Observer {

    private int width;
    private int height;
    private HashMap<Actor, Position> actors;
    private HashMap<Position, Actor> positions;

    public World(int width, int height) {
        this.width = width;
        this.height = height;
        actors = new HashMap<>();
        positions = new HashMap<>();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isOccupied(Position position) {
        return positions.containsKey(position) || !isValid(position);
    }

    public void add(Actor actor) {
        Position position = actor.getPosition();
        if (isValid(position)) {
            actors.put(actor, position);
            positions.put(position, actor);
            actor.addObserver(this);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Actor actor = (Actor) o;
        Position p1 = actors.get(actor);
        Position p2 = actor.getPosition();
        if (isValid(p2) && !isOccupied(p2)) {
            actors.put(actor, p2);
            positions.remove(p1);
            positions.put(p2, actor);
        } else {
            actor.setPosition(p1);
        }
    }

    public boolean isValid(Position position) {
        return 0 <= position.getX() && position.getX() < width
                && 0 <= position.getY() && position.getY() < height;

    }

    public void clear() {
        actors.clear();
        positions.clear();
    }

    public Actor findActor(Position position) {
        return positions.get(position);
    }

}
