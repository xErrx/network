package lan.network.simulation.entities;

public class Genome {

    private final int input;
    private final double weight;
    private final int output;

    public Genome(int input, double weight, int output) {
        this.input = input;
        this.weight = weight;
        this.output = output;
    }

    public int getInput() {
        return input;
    }

    public double getWeight() {
        return weight;
    }

    public int getOutput() {
        return output;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(Integer.toHexString(input));
        builder.append("-");
        builder.append(Long.toHexString(Double.doubleToLongBits(weight)));
        builder.append("-");
        builder.append(Integer.toHexString(output));
        return builder.toString();
    }
    
    
    

    
}
