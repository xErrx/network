package lan.network.simulation.entities;

import static lan.network.simulation.entities.Direction.EAST;
import static lan.network.simulation.entities.Direction.NORTH;
import static lan.network.simulation.entities.Direction.SOUTH;
import static lan.network.simulation.entities.Direction.WEST;

public enum Direction {

    NORTH(Position.UP, Position.DOWN, Position.LEFT, Position.RIGHT),
    EAST(Position.RIGHT, Position.LEFT, Position.UP, Position.DOWN),
    SOUTH(Position.DOWN, Position.UP, Position.RIGHT, Position.LEFT),
    WEST(Position.LEFT, Position.RIGHT, Position.DOWN, Position.UP);

    private final Position forward;
    private final Position backward;
    private final Position left;
    private final Position right;

    private Direction(Position forward, Position backward, Position left, Position right) {
        this.forward = forward;
        this.backward = backward;
        this.left = left;
        this.right = right;
    }

    public Position getForward() {
        return forward;
    }

    public Position getBackward() {
        return backward;
    }

    public Position getLeft() {
        return left;
    }

    public Position getRight() {
        return right;
    }

    public Direction rotateLeft() {
        Direction direction;
        switch (this) {
            case NORTH:
                direction = WEST;
                break;
            case EAST:
                direction = NORTH;
                break;
            case SOUTH:
                direction = EAST;
                break;
            case WEST:
                direction = SOUTH;
                break;
            default:
                throw new RuntimeException("We go to the moon!");
        }
        return direction;
    }

    public Direction rotateRight() {
        Direction direction;
        switch (this) {
            case NORTH:
                direction = EAST;
                break;
            case EAST:
                direction = SOUTH;
                break;
            case SOUTH:
                direction = WEST;
                break;
            case WEST:
                direction = NORTH;
                break;
            default:
                throw new RuntimeException("We go to the moon!");
        }
        return direction;
    }

}
