package lan.network.simulation.inputs;

import lan.network.simulation.entities.Direction;

public class InputDirectionEast extends InputDirection{
    
    public InputDirectionEast() {
        super(Direction.EAST);
    }
    
}
