package lan.network.simulation.inputs;

import lan.network.simulation.entities.Direction;

public class InputDirectionWest extends InputDirection{
    
    public InputDirectionWest() {
        super(Direction.WEST);
    }
    
}
