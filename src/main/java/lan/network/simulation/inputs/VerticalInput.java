package lan.network.simulation.inputs;

import lan.network.simulation.core.InputNode;

public class VerticalInput extends InputNode {

    @Override
    public double calculateValue() {
        return getActor().getPosition().getY() * 1D / (getWorld().getHeight() - 1);
    }

}
