package lan.network.simulation.inputs;

import lan.network.simulation.core.InputNode;
import lan.network.simulation.entities.Position;

public class InputSonar extends InputNode {

    @Override
    public double calculateValue() {
        Position forward = getActor().getDirection().getForward();
        boolean free = true;
        int index = 0;
        while (index < 5 && free) {
            Position next = getActor().getPosition().add(forward);
            if (getWorld().isOccupied(next)) {
                free = false;
            } else {
                index++;
            }
        }
        return index;
    }

}
