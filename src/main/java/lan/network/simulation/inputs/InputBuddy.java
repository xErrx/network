package lan.network.simulation.inputs;

import lan.network.simulation.core.InputNode;
import lan.network.simulation.entities.Actor;

public class InputBuddy extends InputNode {

    @Override
    public double calculateValue() {
        double result;
        Actor buddy = getFramework().findBuddy(getActor());
        if (buddy == null) {
            result = -1D;
        } else {
            if (getActor() == getFramework().findBuddy(buddy)) {
                result = 1D;
            } else {
                result = 0;
            }
        }
        return result;
    }

}
