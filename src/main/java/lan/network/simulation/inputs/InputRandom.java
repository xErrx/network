package lan.network.simulation.inputs;

import java.util.Random;
import lan.network.simulation.core.InputNode;
import lan.network.simulation.entities.Actor;

public class InputRandom extends InputNode {

    private Random random;

    @Override
    public void setActor(Actor actor) {
        super.setActor(actor);
        random = new Random(getActor().getSeed());
    }

    @Override
    public double calculateValue() {
        return random.nextDouble();
    }

}
