package lan.network.simulation.inputs;

import lan.network.simulation.core.InputNode;
import lan.network.simulation.entities.Direction;

public abstract class InputDirection extends InputNode {

    private final Direction direction;

    public InputDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public double calculateValue() {
        return getActor().getDirection() == direction ? 1D : 0D;
    }

  
    
    

}
