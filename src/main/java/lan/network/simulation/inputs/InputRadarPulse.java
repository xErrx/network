package lan.network.simulation.inputs;

import lan.network.simulation.core.InputNode;
import lan.network.simulation.entities.Position;

public class InputRadarPulse extends InputNode {

    private final Position movement;

    public InputRadarPulse(Position movement) {
        this.movement = movement;
    }

    @Override
    public double calculateValue() {
        double value;
        Position position = getActor().getPosition();
        Position target = position.add(movement);
        if (!getWorld().isValid(target)) {
//This seem to bug the system            
//value = -1D;
            value = 0;
        } else if (getWorld().isOccupied(target)) {
            value = 1;
        } else {
            value = 0;
        }
        return value;
    }

}
