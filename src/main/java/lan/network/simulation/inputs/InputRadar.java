package lan.network.simulation.inputs;

import lan.network.simulation.core.InputNode;
import lan.network.simulation.entities.Actor;
import lan.network.simulation.entities.Position;

public class InputRadar extends InputNode {

    @Override
    public double calculateValue() {
        return 0D;
    }

    @Override
    public void setActor(Actor actor) {
        super.setActor(actor);
        int range = getSettings().getRadar();
        for (int i = 0; i < range; i++) {
            int x = i - range;
            for (int j = 0; j < range; j++) {
                int y = j - range;
                Position movement = new Position(x, y);
                InputRadarPulse pulse = new InputRadarPulse(movement);
                pulse.setActor(actor);
                getActor().getBrain().add(pulse); 
            }
        }
    }

}
