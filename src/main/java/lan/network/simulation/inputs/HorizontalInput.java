package lan.network.simulation.inputs;

import lan.network.simulation.core.InputNode;

public class HorizontalInput extends InputNode {

    @Override
    public double calculateValue() {
        return getActor().getPosition().getX() * 1D / (getWorld().getWidth() - 1);
    }

}
