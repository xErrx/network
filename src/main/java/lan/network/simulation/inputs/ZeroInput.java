package lan.network.simulation.inputs;

import lan.network.simulation.core.InputNode;

public class ZeroInput extends InputNode{

    @Override
    public double calculateValue() {
        return 0D;
    }
    
}
