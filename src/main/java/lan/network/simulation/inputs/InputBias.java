package lan.network.simulation.inputs;

import lan.network.simulation.core.InputNode;

public class InputBias extends InputNode{

    @Override
    public double calculateValue() {
        return 1D;
    }
    
}
