package lan.network.simulation.inputs;

import lan.network.simulation.entities.Direction;

public class InputDirectionSouth extends InputDirection{
    
    public InputDirectionSouth() {
        super(Direction.SOUTH);
    }
    
}
