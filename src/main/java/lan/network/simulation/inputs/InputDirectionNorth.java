package lan.network.simulation.inputs;

import lan.network.simulation.entities.Direction;

public class InputDirectionNorth extends InputDirection{
    
    public InputDirectionNorth() {
        super(Direction.NORTH);
    }
    
}
