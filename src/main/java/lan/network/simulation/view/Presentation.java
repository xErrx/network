package lan.network.simulation.view;

import java.awt.Color;
import java.awt.DisplayMode;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import lan.network.simulation.Application;
import lan.network.simulation.entities.Actor;
import lan.network.simulation.entities.Direction;
import lan.network.simulation.entities.Position;
import lan.network.simulation.framework.Framework;

public class Presentation {

    private Framework framework;
    private int side;
    private int boundry;
    private BufferStrategy strategy;
    private List<Color> colors;
    private Map<String, Color> map;
    private int generation;
    private JFrame frame;

    public Presentation(Framework framework) {
        this.framework = framework;
        colors = new LinkedList<>();
        map = new HashMap<>();
        generation = -1;
    }

    public void prepare() {
        GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice device = environment.getDefaultScreenDevice();
        DisplayMode display = device.getDisplayMode();

        int tiles = framework.getWorld().getWidth();
        int separators = tiles + 1;
        int horizontal = (display.getWidth() - separators) / tiles;
        int vertical = (display.getHeight() - separators) / tiles;
        side = Math.min(horizontal, vertical);
        boundry = tiles * side + separators;

        frame = new JFrame("Simulation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(boundry, boundry);
        frame.setUndecorated(true);
        frame.setVisible(true);
        frame.setIgnoreRepaint(true);
        frame.createBufferStrategy(2);
        strategy = frame.getBufferStrategy();

        map = new HashMap<>();

        int population = framework.getSettings().getPopulation();
        int value = 0;
        while(value * value * value < population){
            value++; 
        }
        int amount = 255 / (value + 2);
        for (int i = 0; i < value; i++) {
            for (int j = 0; j < value; j++) {
                for (int k = 0; k < value; k++) {
                    int red = amount + amount * i;
                    int green = amount + amount * j;
                    int blue = amount + amount * k;
                    colors.add(new Color(red, green, blue));
                }
            }
        }
    }

    public void update() {
        if (isActive()) {
            if (framework.getGeneration() != generation) {
                Set<String> marked = new HashSet<>();
                Set<String> unmarked = new HashSet<>();
                for (Actor actor : framework.getActors()) {
                    String code = actor.getCode();
                    Color color = map.get(code);
                    if (color == null) {
                        unmarked.add(code);
                    } else {
                        marked.add(code);
                    }
                }
                Set<String> unused = new HashSet<>(map.keySet());
                unused.removeAll(marked);
                for (String code : unused) {
                    Color color = map.remove(code);
                    colors.add(color);
                }
                for (String code : unmarked) {
                    Color color = colors.remove(0);
                    map.put(code, color);
                }
                generation = framework.getGeneration();
            }
            BufferedImage image = new BufferedImage(boundry, boundry, BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics = image.createGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, boundry, boundry);
            for (Actor actor : framework.getActors()) {
               
                graphics.setColor(map.get(actor.getCode()));

                Position position = actor.getPosition();
                int x = position.getX() * (side + 1) + 1;
                int y = position.getY() * (side + 1) + 1;
                //graphics.fillRect(x, y, side, side);
                
                int a = side / 2;
                int b = side - a;
                
                int max = Math.max(a, b);
                int min = Math.min(a, b);
                
                
                
                Direction direction = actor.getDirection();
                switch (direction) {
                    case NORTH:
                        graphics.fillRect(x + min, y, 1, min);
                        graphics.fillRect(x, y + min, side, max);
                        break;
                        case EAST:
                        graphics.fillRect(x + max, y + min, min, 1);
                        graphics.fillRect(x, y, max, side);
                        break;
                        case SOUTH:
                        graphics.fillRect(x + min, y + max, 1, min);
                        graphics.fillRect(x, y, side, max);                    
                        break;
                        case WEST:
                        graphics.fillRect(x, y + min, min, 1);
                        graphics.fillRect(x + min, y,max, side);
                        break;
                    default:
                        graphics.fillRect(x, y, side, side);
                        break;
                }
                
                
                
                
            }

            Graphics buffer = strategy.getDrawGraphics();
            buffer.drawImage(image, 0, 0, null);
            buffer.dispose();
            strategy.show();
        }

    }

    public void sleep(long amount) {
        if (isActive()) {
            if (amount > 0) {
                try {
                    //System.out.println("Sleep: " + amount);
                    Thread.sleep(amount);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("Simulation is too slow!");
            }
        }
    }

    private boolean isActive() {
       return framework.isActive() || framework.isMilestone();
    }

    public void destroy() {
        frame.dispose();
    }

}
