package lan.network.simulation.filters;

import lan.network.simulation.entities.Actor;

public class BoxFilter implements Filter{
    
    private final int x;
    private final int y;
    private final int w;
    private final int h;

    public BoxFilter(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }
    
    @Override
    public boolean accept(Actor actor) {
        return x <= actor.getPosition().getX() && actor.getPosition().getX() <= x + w - 1
                && y <= actor.getPosition().getY() && actor.getPosition().getY() <= y + h - 1;
       
    }
    
}
