package lan.network.simulation.filters;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import lan.network.simulation.entities.Actor;
import lan.network.simulation.entities.Direction;

public class FilterDirection implements Filter {

    private Set<Direction> directions;

    public FilterDirection(Direction... directions) {
        this.directions = new HashSet<>();
        this.directions.addAll(Arrays.asList(directions));
    }

    @Override
    public boolean accept(Actor actor) {
        return directions.contains(actor.getDirection());
    }
}
