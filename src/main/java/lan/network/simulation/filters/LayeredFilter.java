package lan.network.simulation.filters;

import java.util.ArrayList;
import java.util.List;
import lan.network.simulation.entities.Actor;

public class LayeredFilter implements Filter {

    private final List<Filter> filters;
    private int level;
    private int generation;
    private int survived;

    public LayeredFilter() {
        this.filters = new ArrayList<>();
        this.level = 1;
        this.generation = -1;
        this.survived = -1;
    }

    public void addFilter(Filter filter) {
        filters.add(filter);
    }

    @Override
    public boolean accept(Actor actor) {
        if (generation != actor.getFramework().getGeneration()) {
            generation = actor.getFramework().getGeneration();
            actor.getFramework().setMilestone(false);
            if (1D * survived / actor.getFramework().getSettings().getPopulation() > 0.9D) {
                if (level < filters.size()) {
                    level = level + 1;
                    actor.getFramework().setMilestone(true);
                }
            }
            survived = 0;
        }
        boolean accepted = true;
        int index = 0;
        while (accepted && index < level) {
            Filter filter = filters.get(index++);
            accepted = accepted && filter.accept(actor);
        }
        if(accepted){
            survived++;
        }
        return accepted;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return this.level;
    }

}
