package lan.network.simulation.filters;

import lan.network.simulation.entities.Actor;

public class FilterBuddy implements Filter {

    @Override
    public boolean accept(Actor actor) {
        Actor buddy = actor.getFramework().findBuddy(actor);
        return buddy != null && actor.getFramework().findBuddy(buddy) == actor;
    }

}
