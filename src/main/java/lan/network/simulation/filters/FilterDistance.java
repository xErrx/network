package lan.network.simulation.filters;

import lan.network.simulation.entities.Actor;
import lan.network.simulation.entities.Direction;
import lan.network.simulation.entities.Position;

public class FilterDistance implements Filter {

    @Override
    public boolean accept(Actor actor) {
        Position position = actor.getPosition();
        Direction direction = actor.getDirection();
        Position movement = direction.getForward();
        Position location = position.add(movement);
        return !actor.getFramework().getWorld().isOccupied(location);
    }
}
