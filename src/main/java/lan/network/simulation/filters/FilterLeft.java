package lan.network.simulation.filters;

import lan.network.simulation.entities.Actor;

public class FilterLeft implements Filter {

    private final int threshold;

    public FilterLeft(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public boolean accept(Actor actor) {
        return actor.getPosition().getX() < threshold;
    }
}
